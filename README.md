# README #


### What is this repository for? ###

This repository contains the design and code base for the Path Optimization mobile app.
The app (including its design) is being developed as a part of CSCI 371: Software Engineering at Drury University.


### Organization ###

The Design repository contains design documentation for the project.
The project illustrates development following the Pedal Software Development Process as defined by C. Browning and S. Sigman of Drury University.
The wiki contains the Sprint 1 design documentation for the project.


### Who do I talk to? ###

Contact the repository owner.